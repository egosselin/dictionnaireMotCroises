package com.egs.test.model;

import com.egs.model.Word;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 * <p>
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 * <p>
 * Créé par Elvis Gosselin le 12/05/2019
 */
public class WordTest {

    protected static Word motInstance;

    @BeforeClass
    public static void setUp() {
        motInstance = new Word(1, "tester", "action de tester");
    }

    @Test
    public void testGetIdWillReturnRightValue() {
        assertEquals(motInstance.getId(), 1);
    }

    @Test
    public void testGetMotWillReturnRightValue() {
        assertEquals(motInstance.getWord(), "tester");
    }

    @Test
    public void testGetDefinitionWillReturnRightValue() {
        assertEquals(motInstance.getDefinition(), "action de tester");
    }
}
