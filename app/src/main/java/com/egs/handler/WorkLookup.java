package com.egs.handler;

import android.app.Activity;
import android.inputmethodservice.KeyboardView;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.egs.model.Word;

import java.util.ArrayList;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 11/05/2019
 */

public class WorkLookup extends Handler {

    ListView wordOutputList;
    LinearLayout helpNotice;
    int templateId;
    Activity mainActivity;
    EditText textInput;
    KeyboardView customKeyboard;

    public void setWordOutputList(ListView wordOutputList) {
        this.wordOutputList = wordOutputList;
    }

    public void setHelpNotice(LinearLayout helpNotice) {
        this.helpNotice = helpNotice;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public void setMainActivity(Activity activitePrincipale) {
        this.mainActivity = activitePrincipale;
    }

    public WorkLookup(Looper looper) {
        super(looper);
    }

    public void setTextInput(EditText textInput) {
        this.textInput = textInput;
    }

    public void setCustomKeyboard(KeyboardView customKeyboard) {
        this.customKeyboard = customKeyboard;
    }

    /**
     * Triggered by the click ont the keyboard search button
     *
     * @param msg
     */
    @Override
    public void handleMessage(Message msg) {

        ArrayList<Word> resultats = (ArrayList<Word>) msg.getData().getSerializable("words");

        String toastMessage;

        try {
            int i = 0;

            ArrayList<String> words = new ArrayList<String>();

            for (Word word : resultats) {
                words.add(word.getWord());
                i++;
            }

            toastMessage = i+" mot(s) trouvé(s)";

            ArrayAdapter adaptateur = new ArrayAdapter<String>(mainActivity, templateId, words);

            wordOutputList.setAdapter(adaptateur);

            // reset
            this.resetInput();

            // if no results, show help
            if (i == 0) {
                this.hideShowHelpNotice(true);
            } else {
                this.hideShowHelpNotice(false);
            }
        } catch (NullPointerException e) {
            // text for no results
            toastMessage = "Aucun résultat";
            this.hideShowHelpNotice(true);
        }

        CharSequence toastOutput = toastMessage;

        Toast toast = Toast.makeText(mainActivity, toastOutput, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Reset input field and hide custom keyboard
     */
    protected void resetInput() {
        this.textInput.setText("");
        this.customKeyboard.setVisibility(View.GONE);
        this.customKeyboard.setEnabled(false);
    }

    protected void hideShowHelpNotice(Boolean show) {
        if (show) {
            // hide word list, then show help
            this.helpNotice.setVisibility(View.VISIBLE);
            this.wordOutputList.setVisibility(View.GONE);
        } else {
            // hide help, then show word list
            this.helpNotice.setVisibility(View.GONE);
            this.wordOutputList.setVisibility(View.VISIBLE);
        }
    }
}
