package com.egs.handler;

import android.app.Activity;
import android.inputmethodservice.KeyboardView;
import android.os.Looper;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.egs.dico.R;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 11/05/2019
 */

public class WordLookupFactory {

    public WorkLookup creer(Looper looper, Activity activity) {
        ListView wordOutputList = activity.findViewById(R.id.wordListOuput);
        LinearLayout linearLayout = activity.findViewById(R.id.helpNotice);
        int idTemplateWordList = R.layout.single_row;
        EditText mainTextInput = activity.findViewById(R.id.textInput);
        KeyboardView customKeyboardView = activity.findViewById(R.id.keyboardview);

        WorkLookup wordLookupHandler = new WorkLookup(looper);
        wordLookupHandler.setMainActivity(activity);
        wordLookupHandler.setTemplateId(idTemplateWordList);
        wordLookupHandler.setWordOutputList(wordOutputList);
        wordLookupHandler.setHelpNotice(linearLayout);
        wordLookupHandler.setTextInput(mainTextInput);
        wordLookupHandler.setCustomKeyboard(customKeyboardView);

        return wordLookupHandler;
    }
}
