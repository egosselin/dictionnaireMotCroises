package com.egs.persister;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.egs.model.Database;

import java.io.IOException;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 20/01/2019
 */

public abstract class DatabasePersister {

	protected static SQLiteDatabase sqlite;
	protected Database database;
	protected Context context;

	/**
	 * Construtor (context required)
	 * 
	 * @param context
	 */
	public DatabasePersister(Context context){
		this.context = context;
		// creating database
		database = new Database(this.context);
		
		try {
			database.createDatabase();
		} catch (IOException ioe) {
		    // oh noes!!!1
			throw new Error("Error while creating database");
		}
		
		try {
			database.openDatabase();
		} catch(SQLException sqle) {
			throw sqle;
		}
	}
	
	/**
     * Open database object
	 */
	public void open(){
		sqlite = database.getWritableDatabase();
	}
	
	/**
	 * Close database object
	 */
	public void close(){
		if (sqlite.isOpen()) {
			sqlite.close();
		}
	}
	
	/**
	 * Get database instance
	 * 
	 * @return SQLiteDatabase
	 */
	public SQLiteDatabase getDatabase(){
		return sqlite;
	}
}
