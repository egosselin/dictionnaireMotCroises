package com.egs.keyboard;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 19/10/2019
 */
public class Key {

    public String getKeyStringFromKeyCode(int keyCode) {

        if (keyCode == 1) {
            return "a";
        } else if (keyCode == 2) {
            return "z";
        } else if (keyCode == 3) {
            return "e";
        } else if (keyCode == 4) {
            return "r";
        } else if (keyCode == 5) {
            return "t";
        } else if (keyCode == 6) {
            return "y";
        } else if (keyCode == 7) {
            return "u";
        } else if (keyCode == 8) {
            return "i";
        } else if (keyCode == 9) {
            return "o";
        } else if (keyCode == 10) {
            return "p";
        } else if (keyCode == 11) {
            return "q";
        } else if (keyCode == 12) {
            return "s";
        } else if (keyCode == 13) {
            return "d";
        } else if (keyCode == 14) {
            return "f";
        } else if (keyCode == 15) {
            return "g";
        } else if (keyCode == 16) {
            return "h";
        } else if (keyCode == 17) {
            return "j";
        } else if (keyCode == 18) {
            return "k";
        } else if (keyCode == 19) {
            return "l";
        } else if (keyCode == 20) {
            return "m";
        } else if (keyCode == 21) {
            return "w";
        } else if (keyCode == 22) {
            return "x";
        } else if (keyCode == 23) {
            return "c";
        } else if (keyCode == 24) {
            return "v";
        } else if (keyCode == 25) {
            return "b";
        } else if (keyCode == 26) {
            return "n";
        } else if (keyCode == 27) {
            return "?";
        }

        throw new KeyException("Invalid keyCode " + Integer.toString(keyCode));
    }
}
