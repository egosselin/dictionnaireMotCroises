package com.egs.model;

import java.io.Serializable;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 20/01/2019
 */

public class Word implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private String word;
    private String definition;

    /**
     * Constructor
     *
     * @param long   id
     * @param String word
     * @param String word definition
     */
    public Word(long id, String word, String definition) {
        this.id = id;
        this.word = word;
        this.definition = definition;
    }

    public Word() {

    }

    /**
     * Get de l'id
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * Set de l'id
     *
     * @param id
     * @return
     */
    public Word setId(long id) {
        this.id = id;
        return this;
    }

    /**
     * Word getter
     *
     * @return
     */
    public String getWord() {
        return word;
    }

    /**
     * Word setter
     *
     * @param word
     * @return
     */
    public Word setWord(String word) {
        this.word = word;
        return this;
    }

    /**
     * Get de la définition
     *
     * @return
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * Set de la définition
     *
     * @param definition
     * @return
     */
    public Word setDefinition(String definition) {
        this.definition = definition;
        return this;
    }

    public String toString() {
        return "id : " + this.id + " wrod : " + this.word + " definition : " + this.definition;
    }
}
